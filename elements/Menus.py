from tkinter import *
from Main import *
from childs.plan.planstable import PlansTable as pt
try:
    from childs.plan.report import CheckPlan as cp
except:
    pass
from childs.table import Tables as t
from childs.record import AddRecord as ar
import pandas as pd
from tkinter import filedialog as fd





class Menus(tk.Frame):
    def __init__(self, parent): #current ekzemplyar classa
        super().__init__() #kornevoe okno progi #metod super otiskivaet bazoviy class u classa main, dalee idet obrawenie k metodu init etogo vibrannogo classa
        self.parent = parent
        self.initMenus()

    def initMenus(self):

        self.mainMenu = self.parent.mainMenu

        #Меню файл
        self.fileMenu = Menu(self.mainMenu, tearoff=0)
        self.mainMenu.add_cascade(label="Файл", menu=self.fileMenu)
        self.fileMenu.add_separator()
        # self.filemenu.add_command(label="Открыть...")#, command=_insertText)
        # self.filemenu.add_separator()
        # self.filemenu.add_command(label="Сохранить")#, command=_insertText)
        # self.filemenu.add_separator()
        self.fileMenu.add_command(label="Сохранить как Excel файл", command=self.extractData)
        self.fileMenu.add_separator()
        self.fileMenu.add_command(label="Выход", command=self.parent.closeProgram)
        self.fileMenu.add_separator()

        #Меню запись
        self.recordMenu = Menu(self.mainMenu, tearoff=0)
        self.mainMenu.add_cascade(label="Запись", menu=self.recordMenu)
        self.recordMenu.add_separator()
        self.recordMenu.add_command(label="Добавить запись", command = self.addRecord)
        self.recordMenu.add_separator()
        self.recordMenu.add_command(label="Удалить выбранные записи", command = self.parent.deleteSelectedData)
        self.recordMenu.add_separator()
        self.recordMenu.add_command(label="Удалить все записи", command = self.deleteAllRecords)
        self.recordMenu.add_separator()

        #Менеджер таблиц
        self.mainMenu.add_command(label="Менеджер таблиц", command = self.openTable)


        # Меню план
        self.planMenu = Menu(self.mainMenu, tearoff=0)
        self.mainMenu.add_cascade(label="План", menu=self.planMenu)
        self.planMenu.add_separator()
        self.planMenu.add_command(label="Открыть таблицу запланированных сумм", command=self.viewPlans)
        self.planMenu.add_separator()
        self.planMenu.add_command(label="Проверить выполнение плана", command=self.checkPlan)
        self.planMenu.add_separator()

        #Меню программа
        self.programMenu = Menu(self.mainMenu, tearoff=0)
        self.mainMenu.add_cascade(label="Программа", menu=self.programMenu)
        self.programMenu.add_separator()
        self.programMenu.add_command(label="О программе", command=self.menuAbout)
        self.programMenu.add_separator()

    def viewPlans(self):
        pt.PlansTable(self)

    def checkPlan(self):
        cp.CheckPlan(self)

    def openTable(self):
        t.Tables(self)

    def addRecord(self):
        self.parent.cache_entry.configure(state=tk.NORMAL)

        if self.parent.cache_entry.get()!="":
            ar.AddRecord(self)
        else:
            mb.showerror("Ошибка", "Откройте таблицу, чтобы ввести в неё данные!")

        self.parent.cache_entry.configure(state=tk.DISABLED)


    def deleteAllRecords(self):
        self.parent.cache_entry.configure(state=tk.NORMAL)

        if mb.askyesno("Удаление всех записей", "Вы уверены, что хотите удалить все записи из выбранной таблицы?"):
            if mb.askyesno("Удаление всех записей", "Вы точно уверены? Все данные из выбранной таблицы будут безвозрватно удалены."):
                try:
                    self.parent.db.c.execute('''DELETE FROM '''+ self.parent.cache_entry.get())
                    self.parent.db.conn.commit()
                    self.parent.db.c.execute('''SELECT * FROM ''' + self.parent.cache_entry.get())
                    [self.parent.tree.delete(i) for i in self.parent.tree.get_children()]
                    [self.parent.tree.insert('', 'end', values=row) for row in self.parent.db.c.fetchall()]
                    self.parent.db.conn.commit()
                except:
                    pass
        self.parent.cache_entry.configure(state=tk.DISABLED)

    def extractData(self):
        self.parent.cache_entry.configure(state=tk.NORMAL)

        path = fd.asksaveasfilename(title = "Сохранить как Excel файл",
                                    filetypes=((["XLSX файлы", "*.xlsx"]),),
                                    defaultextension=".xlsx")
        try:
            df = pd.read_sql('''SELECT description, total, date FROM '''+self.parent.cache_entry.get(), sqlite3.connect('dbs/finances.db'))
            writer = pd.ExcelWriter(path, engine='xlsxwriter')
            df.to_excel(excel_writer=writer, sheet_name='Лист 1')
            writer.save()
            writer.close()
        except:
            pass
        self.parent.cache_entry.configure(state=tk.DISABLED)


    def menuAbout(self):
        mb.showinfo("О программе","Создано lbupycajiuml. \nВерсия 1.0.")
