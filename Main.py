import sys, os

import tkinter as tk
from tkinter import ttk
from tkinter import messagebox as mb
import sqlite3
from elements import Menus as m
from childs import EditRecord as er


class Main(tk.Frame):
    def __init__(self, root): #current class instance
        super().__init__(root) #kornevoe okno progi #metod super otiskivaet bazoviy class u classa main, dalee idet obrawenie k metodu init etogo vibrannogo classa
        self.initMain()
        self.db = db

    def initMain(self):

        #Инициализация главного меню
        self.mainMenu = tk.Menu(root)
        root.config(menu=self.mainMenu)
        m.Menus(self)

        #Кэш-поле с именем используемой таблицы
        self.tableName= ttk.Label(self, text='Наименование открытой таблицы')
        self.tableName.grid(row=0, column=0)
        self.cache_entry = ttk.Entry(self, justify = tk.CENTER, foreground="#000000")
        self.cache_entry.grid(row=0, column=1, columnspan=2, ipadx=215, sticky=(tk.N, tk.S, tk.W, tk.E))
        self.cache_entry.configure(state=tk.DISABLED)

        self.tableDescription_label = ttk.Label(self, text='Описание открытой таблицы')
        self.tableDescription_label.grid(row=1, column=0)
        self.tableDescription = tk.Text(self)
        self.tableDescription.grid(row=1, column=1, columnspan=2, ipadx=215, sticky=(tk.N, tk.S, tk.W, tk.E))
        self.tableDescription.configure(height = 3, width = 10)
        self.tableDescription.configure(state=tk.DISABLED)


        #Tree
        self.tree = ttk.Treeview(self, columns=('ID', 'Description', 'Total', 'Date'),
                                 height='20', show='headings')
        #Scrollbar
        self.yscrollbar = ttk.Scrollbar(self, orient=tk.VERTICAL)
        self.yscrollbar.grid(row=2, column=2, sticky=(tk.N, tk.S, tk.W, tk.E))
        self.yscrollbar.config(command=self.tree.yview)
        self.tree.config(yscrollcommand=self.yscrollbar.set)

        #Tree style
        ttk.Style().configure("Treeview", background="#F9FFFF")
        self.tree.column('ID', width=30, anchor = tk.CENTER)
        self.tree.column('Description', width=510)
        self.tree.column('Total', width=140, anchor = tk.CENTER)
        self.tree.column('Date', width=140, anchor = tk.CENTER)
        self.tree.heading('ID', text='№')
        self.tree.heading('Description', text='Наименование')
        self.tree.heading('Total', text='Сумма')
        self.tree.heading('Date', text='Дата')
        self.tree.grid(row=2,column=0, columnspan=2)
        root.bind('<Escape>', lambda event: self.closeProgram())
        self.popupMenu = tk.Menu(tearoff=0)
        self.popupMenu.add_separator()

        self.popupMenu.add_command(label="Копировать", command=self.copy)
        self.popupMenu.add_separator()
        self.popupMenu.add_command(label="Вставить", command=self.paste)
        self.popupMenu.add_separator()
        self.popupMenu.add_command(label="Редактировать" ,command=self.editRecord)
        self.popupMenu.add_separator()
        self.popupMenu.add_command(label="Удалить выделенные записи" ,command=self.deleteSelectedData)
        self.popupMenu.add_separator()

        self.tree.bind("<Button-3>", lambda event: self.popupMenu.tk_popup(event.x_root,event.y_root))
        self.tree.bind("<Button-1>", lambda event: self.popupMenu.unpost())
        self.tree.bind('<Double-Button-1>', lambda event: self.editRecord())

        self.tree.bind('<Control-c>', lambda event: self.copy())
        self.tree.bind('<Control-v>', lambda event: self.paste())

    def closeProgram(self):
        if mb.askyesno("Выход", "Вы действительно хотите выйти?"):
            root.destroy()

    def editRecord(self):
        if str(self.tree.set(self.tree.selection(), '#1')) != "":
            er.EditRecord(self)
        else:
            mb.showerror("Ошибка","Выделите строку для редактирования!")

    def deleteSelectedData(self):
        self.cache_entry.configure(state=tk.NORMAL)

        if mb.askyesno("Удаление записей", "Выделенные записи будут безвозрватно удалены! Удалить выделенные записи? "):
            for selected_item in self.tree.selection():
                self.db.c.execute('''DELETE FROM ''' + self.cache_entry.get() + ''' WHERE id=?''',(self.tree.set(selected_item, '#1'),))
                self.db.conn.commit()
                self.tree.delete(selected_item)
        self.cache_entry.configure(state=tk.DISABLED)


    def paste(self):

        self.db.conn.cursor()
        self.db.c.execute('''INSERT INTO ''' + self.cache_entry.get() + ''' (description, total, date) VALUES(?, ?, ?)''',(self.cache_description, self.cache_total, self.cache_date))
        self.db.conn.commit()

        self.db.c.execute('''SELECT * FROM ''' + self.cache_entry.get())
        [self.tree.delete(i) for i in self.tree.get_children()]
        [self.tree.insert('', 'end', values=row) for row in self.db.c.fetchall()]
        self.db.conn.commit()

    def copy(self):
        if str(self.tree.set(self.tree.selection(), '#1'))!="":
            for selected_item in self.tree.selection():
                self.cache_description = ""
                self.cache_total = ""
                self.cache_date = ""
                self.clipboard_clear()
                self.db.conn.cursor()
                self.db.c.execute('''SELECT description FROM ''' + self.cache_entry.get() + ''' WHERE id = ?''', (self.tree.set(selected_item, '#1'),))
                self.cache_description = str(self.db.c.fetchone())[2:-3]
                self.db.conn.commit()
                self.db.conn.cursor()
                self.db.c.execute('''SELECT total FROM ''' + self.cache_entry.get() + ''' WHERE id = ?''', (self.tree.set(selected_item, '#1'),))
                self.cache_total = str(self.db.c.fetchone())[1:-2]
                self.db.conn.commit()
                self.db.conn.cursor()
                self.db.c.execute('''SELECT date FROM ''' + self.cache_entry.get() + ''' WHERE id = ?''', (self.tree.set(selected_item, '#1'),))
                self.cache_date = str(self.db.c.fetchone())[2:-3]
                self.db.conn.commit()
        else:
            mb.showerror("Ошибка", "Выделите строку для копирования!")

class ConnectDatabase:
    def __init__(self):
        self.conn = sqlite3.connect('dbs/finances.db')
        self.c = self.conn.cursor()
        self.conn.commit()

if __name__ == "__main__":
    root = tk.Tk() #kornevoe okno progi
    db = ConnectDatabase()
    app = Main(root)
    app.grid() #upakovka okna
    root.title("Finances")
    root.geometry("840x500+300+200")
    root.resizable(False, False)
    program_directory = sys.path[0]

    if os.name != 'nt':
        root.iconphoto(True, tk.PhotoImage(file=os.path.join(program_directory, "imgs/root.png")))
    else:
        try:
            root.iconbitmap(os.path.dirname(sys.executable)+'\\imgs\\root.ico')
        except:
            pass
        try:
            root.iconphoto(True, tk.PhotoImage(os.path.dirname(sys.executable)+'\\imgs\\root.png'))
        except:
            pass
    root.mainloop()