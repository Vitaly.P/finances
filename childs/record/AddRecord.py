import tkinter as tk
from tkinter import ttk
import datetime as dt
import sqlite3
import Main

class AddRecord(tk.Toplevel, ttk.Combobox):
    def __init__(self, parent):
        super().__init__()
        self.initChild()
        self.view = Main
        self.parent = parent
        self.db = ConnectDatabase()
        try:
            self.iconbitmap('imgs/childs/record/AddRecord.ico')
        except:
            pass

    def initChild(self):
        self.title('Добавление записи в таблицу')
        self.geometry('400x240+400+300')
        self.resizable(False, False)


        self.months = [u'Январь', u'Февраль', u'Март', u'Апрель', u'Май', u'Июнь', u'Июль',
                       u'Август', u'Сентябрь', u'Октябрь', u'Ноябрь', u'Декабрь']
        self.monthsForCombo = [u'Январь', u'Февраль', u'Март', u'Апрель', u'Май', u'Июнь', u'Июль',
                               u'Август', u'Сентябрь', u'Октябрь', u'Ноябрь', u'Декабрь']

        self.now = dt.date.today()
        self.label_description = tk.Label(self, text='Наименование')
        self.label_description.place(x=50, y=30)
        self.label_sum = tk.Label(self, text='Сумма')
        self.label_sum.place(x=50, y=60)
        self.label_month = tk.Label(self, text='День')
        self.label_month.place(x=50, y=90)
        self.label_month = tk.Label(self, text='Месяц')
        self.label_month.place(x=50, y=120)
        self.label_year = tk.Label(self, text='Год')
        self.label_year.place(x=50, y=150)

        self.titleOfPosition = tk.Entry(self, values=self.setCompletionList(self.getDiscriptions()))

        self.titleOfPosition.place(x=200, y=30, width=143)

        self.entry_money = ttk.Entry(self)
        self.entry_money.place(x=200, y=60,width=143)

        self.day = ttk.Entry(self)
        self.day.place(x=200, y=90,width=143)
        self.day.insert("", self.now.strftime("%d"))
        self.month = ttk.Combobox(self, values=self.monthsForCombo)
        self.month.place(x=200, y=120,width=143)

        self.year = ttk.Entry(self)
        self.year.place(x=200, y=150, width=143)
        self.year.insert(0, dt.date.today().strftime("%Y"))

        for i in range(11):
            if dt.date.today().strftime("%m") == "0" + str(i):
                self.month.delete(0, tk.END)
                self.month.insert(0, self.monthsForCombo.pop(i - 1))

        self.btn_cancel = ttk.Button(self, text='Отмена', command=self.destroy)
        self.btn_cancel.place(x=300, y=190)
        self.btn_add = ttk.Button(self, text='Добавить')
        self.btn_add.place(x=220, y=190)
        self.btn_addAndClear = ttk.Button(self, text='Добавить и очистить')
        self.btn_addAndClear.place(x=60, y=190)
        self.btn_addAndClear.bind('<Button-1>', lambda event: self.addAndClear())

        self.bind('<Return>', lambda event: self.getInputEntries())
        self.btn_add.bind('<Button-1>', lambda event: self.getInputEntries())
        self.bind('<Escape>', lambda event: self.destroy())


    def addAndClear(self):
        self.getInputEntries()
        self.titleOfPosition.delete(0,tk.END)
        self.entry_money.delete(0,tk.END)


    def getInputEntries(self):
        self.date = self.day.get()+'.'+self.convertMonth()+'.'+self.year.get()
        self.insertData(
        self.titleOfPosition.get(),
        self.entry_money.get(),
        self.date)
        self.focus_set()  # даёт приоритет дочернему окну
        self.grab_set()  # берёт все функции, вызванные до этого

    def getDiscriptions(self):
        self.conn = sqlite3.connect('dbs/finances.db')
        self.c = self.conn.cursor()
        tables = []
        self.c.execute('''SELECT name FROM sqlite_master WHERE type = 'table' ''')
        for table in self.c.fetchall():
            tables.append(table[0])
        entries = []
        for i in range(len(tables)):
            self.c.execute('''SELECT description FROM ''' + tables[i])
            for row in self.c.fetchall():
                entries.append(row[0])
            self.conn.commit()
        return entries

    def convertMonth(self):
        convertedMonth = ""
        for i in range(12):
            if self.months.index('' + self.month.get() + '') == i:
                convertedMonth = str(i + 1)
        return convertedMonth

    def insertData(self, description, total, date):
        self.parent.parent.cache_entry.configure(state=tk.NORMAL)

        self.db.c.execute('''CREATE TABLE IF NOT EXISTS ''' + self.parent.parent.cache_entry.get() + ''' (id integer primary key, description text, total real, date date)''')
        self.db.c.execute('''INSERT INTO '''+ self.parent.parent.cache_entry.get() +''' (description, total, date) VALUES(?, ?, ?)''', (description, total, date))
        self.db.conn.commit()
        self.db.c.execute('''SELECT * FROM ''' + self.parent.parent.cache_entry.get())
        [self.parent.parent.tree.delete(i) for i in self.parent.parent.tree.get_children()]
        [self.parent.parent.tree.insert('', 'end', values=row) for row in self.db.c.fetchall()]
        self.db.conn.commit()
        self.parent.parent.cache_entry.configure(state=tk.DISABLED)


    def setCompletionList(self, completion_list):
        try:
            self._completion_list = sorted(completion_list, key=str.lower)  # Work with a sorted list
        except:
            pass
        self._hits = []
        self._hit_index = 0
        self.position = 0
        self.bind('<KeyRelease>', self.handleKeyrelease)
    def autoComplete(self, delta=0):
        if delta:  # need to delete selection otherwise we would fix the current position
            self.titleOfPosition.delete(self.position, tk.END)
        else:  # set position to end so selection starts where textentry ended
            self.position = len(self.titleOfPosition.get())
        # collect hits
        _hits = []
        for element in self._completion_list:
            if self.titleOfPosition.get().lower() in element.lower():  # Match case insensitively
                _hits.append(element)
        # if we have a new hit list, keep this in mind
        if _hits != self._hits:
            self._hit_index = 0
            self._hits = _hits
        # only allow cycling if we are in a known hit list
        if _hits == self._hits and self._hits:
            self._hit_index = (self._hit_index + delta) % len(self._hits)
        # now finally perform the auto completion
        if self._hits:
            self.titleOfPosition.delete(0, tk.END)
            self.titleOfPosition.insert(0, self._hits[self._hit_index])
            self.titleOfPosition.select_range(self.position, tk.END)

    def handleKeyrelease(self, event):
        if event.keysym == "BackSpace":
            self.titleOfPosition.delete(self.titleOfPosition.index(tk.INSERT), tk.END)
            self.titleOfPosition.position = self.titleOfPosition.index(tk.END)
        # if event.keysym == "Left":
        #     if self.titleOfPosition.position < self.titleOfPosition.index(tk.END):  # delete the selection
        #         self.titleOfPosition.delete(self.titleOfPosition.position, tk.END)
        #     else:
        #         self.titleOfPosition.position = self.titleOfPosition.position - 1  # delete one character
        #         self.titleOfPosition.delete(self.titleOfPosition.position, tk.END)
        if event.keysym == "Right":
            self.titleOfPosition.position = self.titleOfPosition.index(tk.END)  # go to end (no selection)
        if len(event.keysym) == 1:
            self.autoComplete()

class ConnectDatabase:
    def __init__(self):
        self.conn = sqlite3.connect('dbs/finances.db')
        self.c = self.conn.cursor()
        self.conn.commit()