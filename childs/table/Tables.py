from Main import *
from childs.table import CreateTable as ct

class Tables(tk.Toplevel):
    def __init__(self, parent):
        super().__init__()
        self.initChild()
        self.queryViewTables()
        self.parent = parent
        try:
            self.iconbitmap('imgs/childs/table/Tables.ico')
        except:
            pass


    def initChild(self):
        self.title('Менеджер таблиц')
        self.geometry('628x350+400+300')
        self.resizable(False, False)

        self.tree = ttk.Treeview(self, y=50, columns=('Table'),
                                 height=15, show='headings')

        #Scrollbar
        self.yscrollbar = ttk.Scrollbar(self, orient=tk.VERTICAL)
        self.yscrollbar.grid(row=1, column=3, sticky=(tk.N, tk.S, tk.W, tk.E))
        self.yscrollbar.config(command=self.tree.yview)
        self.tree.config(yscrollcommand=self.yscrollbar.set)
        self.tree.column('Table', width=612, anchor=tk.CENTER)
        self.tree.heading('Table', text='Наименование таблицы')
        self.tree.grid(row = 1, column = 0, columnspan=3)
        self.tree.bind('<Double-Button-1>', lambda event: self.queryOpenTable())

        self.add = tk.Button(self, text='Открыть выбранную таблицу',
                                                  bg='#d7d8e0',
                                                  bd=0.5)
        self.add.grid (row = 0, column = 0, sticky=(tk.N, tk.S, tk.W, tk.E))
        self.add.bind('<Button-1>', lambda event: self.queryOpenTable(), )

        self.create = tk.Button(self, text='Создать таблицу',
                                bg='#d7d8e0',
                                bd=0.5)
        self.create.grid(row=0, column=1, sticky=(tk.N, tk.S, tk.W, tk.E))
        self.create.bind('<Button-1>', lambda event: self.createTable())

        self.remove = tk.Button(self, text='Удалить выбранную таблицу',
                             bg='#d7d8e0',
                             bd=0.5)
        self.remove.grid(row=0, column=2,columnspan=2, sticky=(tk.N, tk.S, tk.W, tk.E))
        self.remove.bind('<Button-1>', lambda event: self.queryDeleteTable())



        self.bind('<Return>', lambda event: self.queryOpenTable())
        self.bind('<Escape>', lambda event: self.destroy())

    def queryViewTables(self):
        self.conn = sqlite3.connect('dbs/finances.db')
        self.c = self.conn.cursor()
        self.c.execute('''SELECT name FROM sqlite_master INFORMATION_SCHEMA WHERE type='table' ''')
        [self.tree.delete(i) for i in self.tree.get_children()]
        [self.tree.insert('', 'end', values=row) for row in self.c.fetchall()]
        self.conn.commit()

    def queryOpenTable(self):
        self.parent.parent.cache_entry.configure(state=tk.NORMAL)
        self.parent.parent.tableDescription.configure(state=tk.NORMAL)

        self.parent.parent.tableDescription.delete(1.0, tk.END)
        self.parent.parent.cache_entry.delete(0,"")
        self.conn = sqlite3.connect('dbs/finances.db')
        self.c = self.conn.cursor()
        for selected_item in self.tree.selection():
            self.c.execute('''SELECT * FROM ''' + str(self.tree.set(selected_item, '#1')))
            self.parent.parent.cache_entry.insert(0, str(self.tree.set(selected_item, '#1')))
            [self.parent.parent.tree.delete(i) for i in self.parent.parent.tree.get_children()]
            [self.parent.parent.tree.insert('', 'end', values=row) for row in self.c.fetchall()]
            self.conn.commit()

            self.conn = sqlite3.connect('dbs/descriptions.db')
            self.c = self.conn.cursor()
            self.c.execute('''SELECT description FROM ''' + str(self.tree.set(selected_item, '#1')))
            self.parent.parent.tableDescription.insert(1.0,str(self.c.fetchone())[2:-5])
            self.conn.commit()

        self.destroy()
        self.parent.parent.tableDescription.configure(state=tk.DISABLED)

        self.parent.parent.cache_entry.configure(state=tk.DISABLED)


    def queryDeleteTable(self):
        self.parent.parent.cache_entry.configure(state=tk.NORMAL)
        self.parent.parent.tableDescription.configure(state=tk.NORMAL)

        self.dbFinances = sqlite3.connect('dbs/finances.db')
        self.financesCursor = self.dbFinances.cursor()
        self.dbPlans = sqlite3.connect('dbs/plans.db')
        self.plansCursor = self.dbPlans.cursor()
        self.dbDescriptions = sqlite3.connect('dbs/descriptions.db')
        self.descriptionsCursor = self.dbDescriptions.cursor()
        if mb.askokcancel("Удаление таблицы", "Вы уверены?")==True:
            if mb.askokcancel("Удаление всех записей", "Вы точно уверены? Все данные будут безвозрватно удалены.")==True:
                self.financesCursor.execute('''DROP TABLE ''' + str(self.tree.set(self.tree.selection(), '#1')))
                self.dbFinances.commit()
                try:
                    if str(self.tree.set(self.tree.selection(), '#1')) == self.parent.parent.cache_entry.get():
                        [self.parent.parent.tree.delete(i) for i in self.parent.parent.tree.get_children()]
                        self.parent.parent.cache_entry.delete(0, "")
                        self.parent.parent.tableDescription.delete(1.0,tk.END)
                    self.plansCursor.execute('''DROP TABLE ''' + str(self.tree.set(self.tree.selection(), '#1')))
                    self.dbPlans.commit()
                    self.descriptionsCursor.execute('''DROP TABLE ''' + str(self.tree.set(self.tree.selection(), '#1')))
                    self.dbDescriptions.commit()
                except:
                    pass
                self.focus_set()
        self.queryViewTables()
        self.parent.parent.tableDescription.configure(state=tk.DISABLED)
        self.parent.parent.cache_entry.configure(state=tk.DISABLED)


    def createTable(self):
        ct.CreateTable(self)
