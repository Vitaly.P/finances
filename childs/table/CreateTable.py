import re
from Main import *
import datetime as dt


class CreateTable(tk.Toplevel):
    def __init__(self, parent):
        super().__init__()
        self.initChild()
        self.parent = parent
        try:
            self.iconbitmap('imgs/childs/table/CreateTable.ico')
        except:
            pass

    def initChild(self):
        self.title('Новая таблица')
        self.geometry('420x245+400+300')
        self.resizable(False, False)

        #Table name
        self.labelName = tk.Label(self, text='Имя таблицы')
        self.labelName.place(x=10, y=15)
        self.entryNameOfTable = ttk.Entry(self)
        self.entryNameOfTable.place(x=200, y=15, width=200)

        #Plan
        self.labelPlan = tk.Label(self, text='Запланированная сумма')
        self.labelPlan.place(x=10, y=45)
        self.entrySumOfPlan = ttk.Entry(self)
        self.entrySumOfPlan.place(x=200, y=45, width=200)

        # Description
        self.labelDescription = tk.Label(self, text='Описание таблицы')
        self.labelDescription.place(x=10, y=75)
        self.textDescription = tk.Text(self)
        self.textDescription.place(x=200, y=75, width=200, height=110)

        #Buttons
        self.buttonCancel = ttk.Button(self, text='Отмена', command=self.destroy)
        self.buttonCancel.place(x=315, y=205)
        self.buttonCreate = ttk.Button(self, text='Создать')
        self.buttonCreate.place(x=230, y=205)
        self.buttonCreate.bind('<Button-1>', lambda event: self.getName())

        self.bind('<Escape>', lambda event: self.destroy())

    def getName(self):
        namePattern = r'[^\.a-zA-Zа-яА-Я_]'
        planPattern = r'[^\.0-9_]'
        self.conn = sqlite3.connect('dbs/finances.db')
        self.c = self.conn.cursor()
        self.c.execute('''SELECT name FROM sqlite_master INFORMATION_SCHEMA WHERE type='table' ''')
        listOfTables = self.c.fetchall()
        self.conn.commit()


        if re.search(namePattern, self.entryNameOfTable.get()):
            mb.showerror("Ошибка", '''Введите корректное имя. Имя может содержать следуюшие символы "a-z, A-Z, а-я, А-Я, _".''')
        else:
            if re.search(planPattern, self.entrySumOfPlan.get()):
                mb.showerror("Ошибка", '''Введите корректную сумму. Сумма может содержать следуюшие символы "0-9".''')
            else:

                if str(listOfTables).__contains__(self.entryNameOfTable.get()):
                    mb.showerror("Ошибка","Таблица с таким именем уже существует, используйте другое имя для таблицы!")
                else:
                    self.parent.parent.parent.db.c.execute('''CREATE TABLE IF NOT EXISTS ''' + self.entryNameOfTable.get() + ''' (id integer primary key, description text, total real, date date)''')
                    self.parent.parent.parent.db.conn.commit()

                    if self.entrySumOfPlan.get()!="":
                        self.insertPlan(self.entryNameOfTable.get(), self.entrySumOfPlan.get())
                        if self.textDescription.get('1.0', "end")!="":
                            self.insertDescription(self.entryNameOfTable.get(),self.textDescription.get('1.0', "end"))
                        self.openNewTable()
                    else:
                        mb.showinfo("Внимание!","Таблица создана, но Вы не ввели запланированную сумму. Вы можете ввести её в...")
                        self.insertPlanWithoutSum(self.entryNameOfTable.get())
                        self.openNewTable()


    def insertPlan(self, tableName, plan):
        self.date = dt.date.today().strftime("%m.%Y")[1:]
        self.connect = sqlite3.connect('dbs/plans.db')
        self.cursor = self.connect.cursor()
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS '''+tableName+''' (id integer primary key, date text, sumOfPlan real)''')
        self.cursor.execute('''INSERT INTO ''' +tableName+ ''' (date, sumOfPlan) VALUES (?, ?)''', (self.date, plan))
        self.connect.commit()
    def insertPlanWithoutSum(self, tableName):
        self.connect = sqlite3.connect('dbs/plans.db')
        self.cursor = self.connect.cursor()
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS '''+tableName+''' (id integer primary key, date text, sumOfPlan real)''')
        self.connect.commit()
    def insertDescription(self, tableName, description):
        self.connect = sqlite3.connect('dbs/descriptions.db')
        self.cursor = self.connect.cursor()
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS '''+tableName+''' (id integer primary key, description text)''')
        self.cursor.execute('''INSERT INTO ''' +tableName+ ''' (description) VALUES (?)''', (description,))
        self.connect.commit()

    def openNewTable(self):
        self.msgBox = mb.askokcancel("Открыть таблицу", "Открыть только что созданную таблицу?")
        if self.msgBox == True:
            self.connect = sqlite3.connect('dbs/finances.db')
            self.connect.execute('''SELECT * FROM ''' + self.entryNameOfTable.get())
            self.connect.commit()
            [self.parent.tree.delete(i) for i in self.parent.tree.get_children()]
            self.parent.parent.parent.cache_entry.configure(state=tk.NORMAL)
            self.parent.parent.parent.cache_entry.delete(0, "")
            self.parent.parent.parent.cache_entry.insert(0, self.entryNameOfTable.get())
            self.parent.parent.parent.cache_entry.configure(state=tk.DISABLED)
            try:
                self.parent.parent.parent.tableDescription.configure(state=tk.NORMAL)
                self.conn = sqlite3.connect('dbs/descriptions.db')
                self.c = self.conn.cursor()
                self.c.execute('''SELECT description FROM ''' + self.entryNameOfTable.get())
                self.parent.parent.parent.tableDescription.insert(1.0,str(self.c.fetchone()))#[-2:-5])
                self.conn.commit()
                self.parent.parent.parent.tableDescription.configure(state=tk.DISABLED)
            except:
                pass
            self.parent.queryViewTables()

            self.grab_set()
            self.focus_set()
            self.destroy()
            self.parent.destroy()
        else:
            self.parent.queryViewTables()






