from tkinter import *
from Main import *

class EditRecord(Toplevel):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent

        self.initChild()
        try:
            self.iconbitmap('imgs/childs/EditRecord.ico')
        except:
            pass

    def initChild(self):
        self.title('Редактирование строки')
        self.geometry('575x80+400+300')
        self.resizable(False, False)

        #Labels
        self.titleOfPosition_label = Label(self, text='Наименование')
        self.titleOfPosition_label.grid(row=0, column=0)
        self.sum_label = Label(self, text='Сумма')
        self.sum_label.grid(row=0, column=1)
        self.date_label = Label(self, text='Дата')
        self.date_label.grid(row=0, column=2)

        #Entries
        self.titleOfPosition_entry = ttk.Entry(self, width=50)
        self.titleOfPosition_entry.grid(row=1, column=0)
        self.sum_entry = ttk.Entry(self, width=10)
        self.sum_entry.grid(row=1, column=1)
        self.date_entry = ttk.Entry(self, width=10)
        self.date_entry.grid(row=1, column=2)

        #Buttons
        self.btn_cancel = ttk.Button(self, text='Отмена', command=self.destroy)
        self.btn_cancel.grid(row=2, column=2,sticky=(tk.N, tk.S, tk.W, tk.E))
        self.btn_update = ttk.Button(self, text='Сохранить')
        self.btn_update.grid(row=2, column=1,sticky=(tk.N, tk.S, tk.W, tk.E))
        self.bind('<Escape>', lambda event: self.destroy())
        self.bind('<Return>', lambda event: self.updateRecord())
        self.btn_update.bind('<Button-1>', lambda event:self.updateRecord())
        try:
            self.focus_set()
            self.grab_set()
        except:
            pass
        self.getRecord()

    def getRecord(self):
        self.conn = sqlite3.connect('dbs/finances.db')
        self.c = self.conn.cursor()
        self.c.execute('''SELECT description FROM ''' + self.parent.cache_entry.get()+ ''' WHERE id = ''' + str(self.parent.tree.set(self.parent.tree.selection(), '#1')))
        self.titleOfPosition_entry.insert("", str(self.c.fetchone())[2:-3])
        self.c.execute('''SELECT total FROM ''' + self.parent.cache_entry.get()+ ''' WHERE id = ''' + str(self.parent.tree.set(self.parent.tree.selection(), '#1')))
        self.sum_entry.insert("", str(self.c.fetchone())[1:-2])
        self.c.execute('''SELECT date FROM ''' + self.parent.cache_entry.get() + ''' WHERE id = ''' + str(self.parent.tree.set(self.parent.tree.selection(), '#1')))
        self.date_entry.insert("", str(self.c.fetchone())[2:-3])
        self.conn.commit()

    def queryOpenTable(self):
        self.conn = sqlite3.connect('dbs/finances.db')
        self.c = self.conn.cursor()
        self.c.execute('''SELECT * FROM ''' + self.parent.cache_entry.get())
        [self.parent.tree.delete(i) for i in self.parent.tree.get_children()]
        [self.parent.tree.insert('', 'end', values=row) for row in self.c.fetchall()]
        self.conn.commit()

    def updateRecord(self):
        titlePattern = r'[^\.0-9a-zA-Zа-яА-Я_]'
        sumPattern = r'[^\.0-9]'
        datePattern = r'[^\.0-9]'
        if re.search(titlePattern, self.titleOfPosition_entry.get()) or self.titleOfPosition_entry.get() == '':
            mb.showerror("Ошибка",
                         '''Введите корректное наименование. Наименование может содержать следуюшие символы "0-9, a-z, A-Z, а-я, А-Я, _, .".''')
        elif re.search(sumPattern, self.sum_entry.get()) or self.sum_entry.get() == '':
            mb.showerror("Ошибка",
                         '''Введите корректную сумму. Сумма может содержать следуюшие символы "0-9, .".''')
        elif re.search(datePattern, self.date_entry.get()) or self.date_entry.get() == '':
            mb.showerror("Ошибка",
                         '''Введите корректную дату. Дата может содержать следуюшие символы "0-9, .".''')
        else:
            self.conn = sqlite3.connect('dbs/finances.db')
            self.c = self.conn.cursor()
            self.c.execute('''UPDATE ''' + self.parent.cache_entry.get() + ''' SET description = "''' + self.titleOfPosition_entry.get() +'''" WHERE id = ''' + str(self.parent.tree.set(self.parent.tree.selection(), '#1')))
            self.c.execute('''UPDATE ''' + self.parent.cache_entry.get() + ''' SET total = ''' + self.sum_entry.get() +''' WHERE id = ''' + str(self.parent.tree.set(self.parent.tree.selection(), '#1')))
            self.c.execute('''UPDATE ''' + self.parent.cache_entry.get() + ''' SET date = "''' + self.date_entry.get() +'''" WHERE id = ''' + str(self.parent.tree.set(self.parent.tree.selection(), '#1')))
            self.conn.commit()
            self.queryOpenTable()
            self.destroy()

