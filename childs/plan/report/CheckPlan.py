from tkinter import *
from tkinter import ttk, messagebox as mb
import datetime as dt
import Main
from childs.plan.report import ViewReport as vr
import sqlite3

class CheckPlan(Toplevel):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent

        self.initChild()
        self.view = Main
        try:
            self.iconbitmap('imgs/childs/plan/CheckPlan.ico')
        except:
            pass

    def initChild(self):
        self.title('Проверка выполнения плана')
        self.geometry('480x140+400+300')
        self.resizable(False, False)

        self.label_of_table_name = Label(self, text='Наименование таблицы:')
        self.label_of_table_name.place(x=35, y=20)
        self.tableNamesCombobox = ttk.Combobox(self, values=[self.setCompletionList(self.getTableNames())])
        self.tableNamesCombobox.config(values=self.getTableNames())
        try:
            self.tableNamesCombobox.current(0)
        except:
            pass
        self.tableNamesCombobox.place(x=230, y=20, width=200)

        self.label_month = Label(self, text='Месяц')
        self.label_month.place(x=165, y=50)
        self.label_year = Label(self, text='Год')
        self.label_year.place(x=345, y=50)
        self.months = [u'Январь', u'Февраль', u'Март', u'Апрель', u'Май', u'Июнь', u'Июль',
                                                   u'Август', u'Сентябрь', u'Октябрь', u'Ноябрь', u'Декабрь']
        self.monthsForCombo = [u'Январь', u'Февраль', u'Март', u'Апрель', u'Май', u'Июнь', u'Июль',
                                                   u'Август', u'Сентябрь', u'Октябрь', u'Ноябрь', u'Декабрь']
        self.month = ttk.Combobox(self, values=self.monthsForCombo)
        self.month.current(0)
        self.month.place(x=230, y=50, width=110)

        for i in range (11):
            if dt.date.today().strftime("%m")=="0"+str(i):
                self.month.delete(0,END)
                self.month.insert(0, self.monthsForCombo.pop(i-1))

        self.year = ttk.Entry(self)
        self.year.place(x=380, y=50,width=50)
        self.year.insert(0, dt.date.today().strftime("%Y"))
        self.btn_cancel = ttk.Button(self, text='Отмена', command=self.destroy)
        self.btn_cancel.place(x=365, y=90)
        self.btn_check = ttk.Button(self, text='Проверить')
        self.btn_check.place(x=270, y=90)
        self.now = dt.date.today()

        #!!!!!!!!!!!!
        self.btn_check.bind('<Button-1>', lambda event:self.checkPlan())
        self.bind('<Escape>', lambda event: self.destroy())
        self.bind('<Return>', lambda event: self.checkPlan())

    def getSumOfPlan(self, tableName):
        self.conn = sqlite3.connect('dbs/plans.db')
        self.c = self.conn.cursor()
        self.c.execute('''SELECT sumOfPlan FROM ''' +tableName+ ''' WHERE date LIKE '%'''  + self.date+'''%' ''')
        sumOfPlan = self.c.fetchall()
        self.conn.commit()
        return sumOfPlan.__str__()[2:-3]

    def convertMonth(self):
        convertedMonth = ""
        for i in range (12):
            if self.months.index(''+self.month.get()+'') == i:
                convertedMonth = str(i+1)
        return convertedMonth


    def getTableNames(self):
        self.conn = sqlite3.connect('dbs/finances.db')
        self.c = self.conn.cursor()
        tables = []
        self.c.execute('''SELECT name FROM sqlite_master WHERE type = 'table' ''')
        for table in self.c.fetchall():
            tables.append(table[0])
        return tables
    def checkPlan(self):
        self.date = self.convertMonth()+'.'+str(self.year.get())
        if self.tableNamesCombobox.get() != "":
            try:
                self.sumCheck = self.getSumOfPlan(self.tableNamesCombobox.get())
            except:
                pass
            if self.sumCheck != "":
                self.conn = sqlite3.connect('dbs/finances.db')
                self.c = self.conn.cursor()
                self.c.execute(
                    '''SELECT SUM(total) FROM ''' + self.tableNamesCombobox.get() + ''' WHERE date LIKE '%''' + self.date + '''%' ''')
                self.reportCheck = str(self.c.fetchall())
                self.conn.commit()
                if self.reportCheck==("[(None,)]"):
                    mb.showerror("Ошибка", "Отсутствуют записи в таблице за выбранный вами месяц!")
                else:
                    vr.ViewReport.viewReport(vr.ViewReport(self),
                                             self.sumCheck, self.date, self.tableNamesCombobox.get())
                    self.grab_set()  # берёт все функции, вызванные до этого
                    self.focus_set()  # даёт приоритет дочернему окну
                    self.destroy()
            else:
                mb.showerror("Ошибка", "На выбранный вами месяц отсутствует запланированная сумма!")
        else:
            mb.showerror("Ошибка", 'Введите имя таблицы в поле "Наименование таблицы", чтобы сделать запрос!')

    def setCompletionList(self, completion_list):
        self._completion_list = sorted(completion_list, key=str.lower)  # Work with a sorted list
        self._hits = []
        self._hit_index = 0
        self.position = 0
        self.bind('<KeyRelease>', self.handleKeyrelease)

    def autoComplete(self, delta=0):
        if delta: # need to delete selection otherwise we would fix the current position
                self.tableNamesCombobox.delete(self.position, END)
        else: # set position to end so selection starts where textentry ended
                self.position = len(self.tableNamesCombobox.get())
        # collect hits
        _hits = []
        for element in self._completion_list:
                if self.tableNamesCombobox.get().lower() in element.lower(): # Match case insensitively
                        _hits.append(element)
        # if we have a new hit list, keep this in mind
        if _hits != self._hits:
                self._hit_index = 0
                self._hits=_hits
        # only allow cycling if we are in a known hit list
        if _hits == self._hits and self._hits:
                self._hit_index = (self._hit_index + delta) % len(self._hits)
        # now finally perform the auto completion
        if self._hits:
                self.tableNamesCombobox.delete(0, END)
                self.tableNamesCombobox.insert(0,self._hits[self._hit_index])
                self.tableNamesCombobox.select_range(self.position,END)

    def handleKeyrelease(self, event):
        """event handler for the keyrelease event on this widget"""
        if event.keysym == "BackSpace":
                self.tableNamesCombobox.delete(self.tableNamesCombobox.index(INSERT), END)
                self.position = self.tableNamesCombobox.index(END)
        # if event.keysym == "Left":
        #         if self.position < self.tableNamesCombobox.index(END): # delete the selection
        #                 self.tableNamesCombobox.delete(self.position, END)
        #         else:
        #                 self.position = self.position-1 # delete one character
        #                 self.tableNamesCombobox.delete(self.position, END)
        if event.keysym == "Right":
                self.position = self.tableNamesCombobox.index(END) # go to end (no selection)
        if len(event.keysym) == 1:
                self.autoComplete()