from Main import *
from tkinter import *
import datetime as dt
import pandas as pd


class ViewReport(Toplevel):
    def __init__(self, parent):
        super().__init__()
        self.initChild()
        self.parent = parent

        try:
            self.iconbitmap('imgs/childs/plan/ViewReport.ico')
        except:
            pass

    def initChild(self):
        self.title('Отчёт от '+dt.date.today().strftime("%d.%m.%Y"))
        self.geometry("840x400+300+200")

        self.resizable(False, False)

        self.mainmenu = tk.Menu()
        self.config(menu=self.mainmenu)

        # Меню отчёт
        self.filemenu = tk.Menu(self.mainmenu, tearoff=0)
        self.mainmenu.add_cascade(label="Отчёт", menu=self.filemenu)
        self.filemenu.add_command(label="Закрыть отчёт", command=self.closeReport)

        #Tree
        self.tree = ttk.Treeview(self, columns=('ID', 'Description', 'Total', 'Date'),
                                 height=22, show='headings')
        #Scrollbar
        self.yscrollbar = ttk.Scrollbar(self, orient=tk.VERTICAL)
        self.yscrollbar.pack(side=tk.RIGHT, fill=tk.Y)
        self.yscrollbar.config(command=self.tree.yview)
        self.tree.config(yscrollcommand=self.yscrollbar.set)

        #Tree style
        ttk.Style().configure("Treeview", background="#F9FFFF")
        self.tree.column('ID', width=30, anchor = tk.CENTER)
        self.tree.column('Description', width=510)
        self.tree.column('Total', width=140, anchor = tk.CENTER)
        self.tree.column('Date', width=140, anchor = tk.CENTER)
        self.tree.heading('ID', text='№')
        self.tree.heading('Description', text='Наименование')
        self.tree.heading('Total', text='Сумма')
        self.tree.heading('Date', text='Дата')
        self.tree.pack()
        self.bind('<Escape>', lambda event: self.destroy())

        #!!!!!!!!!!!!!!!!!!!!!!
        #self.grab_set()
        #!!!!!!!!!!!!!!!!!!!!!!

        self.focus_set()  # даёт приоритет дочернему окну


    def closeReport(self):
        if mb.askyesno("Закрыть отчёт", "Вы действительно хотите закрыть отчёт?"):
            self.destroy()

    def viewReport(self, sum, date, tableName):
        self.sharedDate = date
        self.parent.parent.parent.db.c.execute('''SELECT * FROM ''' +tableName+ ''' WHERE date LIKE '%'''  + date+'''%' ''')
        [self.tree.delete(i) for i in self.tree.get_children()]
        [self.tree.insert('', 'end', values=row) for row in self.parent.parent.parent.db.c.fetchall()]
        self.parent.parent.parent.db.conn.commit()
        self.parent.parent.parent.db.c.execute('''SELECT SUM(total) FROM ''' + tableName + ''' WHERE date LIKE '%'''+ date+'''%' ''')
        sumFromBase = float(str(self.parent.parent.parent.db.c.fetchone())[1:-2])
        if sumFromBase>float(sum):
            self.tree.insert('','end',values=['',"Сумма затрат = "+str(sumFromBase)+" >"+" запланированной суммы = "+sum+". План не выполнен.","Расхождение: "," -"+str(sumFromBase-(float(sum)))])
        else:
            self.tree.insert('','end',values=['',"Сумма затрат = "+str(sumFromBase)+" <"+" запланированной суммы = "+sum+". План выполнен.","Сэкономлено:"," "+str(abs(sumFromBase-(float(sum))))])
        self.parent.parent.parent.db.conn.commit()



