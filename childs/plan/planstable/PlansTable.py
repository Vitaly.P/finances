from Main import *
from childs.plan.planstable import ViewPlan as vp


class PlansTable(tk.Toplevel):
    def __init__(self, parent):
        super().__init__()
        self.initChild()
        self.queryViewTables()
        self.parent = parent
        try:
            self.iconbitmap('imgs/childs/plan/PlansTable.ico')
        except:
            pass

    def initChild(self):
        self.title('Выберите таблицу с запланированными суммами')
        self.geometry('500x345+400+300')
        self.resizable(False, False)

        self.tree = ttk.Treeview(self, y=50, columns=('Table'),
                                 height=15, show='headings')

        #Scrollbar
        self.yscrollbar = ttk.Scrollbar(self, orient=tk.VERTICAL)
        self.yscrollbar.grid(row=1, column=2, sticky=(tk.N, tk.S, tk.W, tk.E))
        self.yscrollbar.config(command=self.tree.yview)
        self.tree.config(yscrollcommand=self.yscrollbar.set)
        self.tree.column('Table', width=482, anchor=tk.CENTER)
        self.tree.heading('Table', text='Наименование таблицы')
        self.tree.grid(row = 1, column = 0, columnspan=2)
        self.tree.bind('<Double-Button-1>', lambda event: self.viewPlan())

        self.open = tk.Button(self, text='Открыть выбранную таблицу',
                                                  bg='#d7d8e0',
                                                  bd=0.5)
        self.open.grid (row = 0, column = 0, columnspan=2, sticky=(tk.N, tk.S, tk.W, tk.E))
        self.open.bind('<Button-1>', lambda event: self.viewPlan())


        self.bind('<Return>', lambda event: self.viewPlan())
        self.bind('<Escape>', lambda event: self.destroy())

    def queryViewTables(self):
        self.conn = sqlite3.connect('dbs/plans.db')
        self.c = self.conn.cursor()
        self.c.execute('''SELECT name FROM sqlite_master INFORMATION_SCHEMA WHERE type='table' ''')
        [self.tree.delete(i) for i in self.tree.get_children()]
        [self.tree.insert('', 'end', values=row) for row in self.c.fetchall()]
        self.conn.commit()

    def queryOpenTable(self):
        self.parent.cache_entry.configure(state=tk.NORMAL)

        self.parent.cache_entry.delete(0,"")
        self.conn = sqlite3.connect('dbs/plans.db')
        self.c = self.conn.cursor()
        for selected_item in self.tree.selection():
            self.c.execute('''SELECT * FROM ''' + str(self.tree.set(selected_item, '#1')))
            self.parent.cache_entry.insert(0, str(self.tree.set(selected_item, '#1')))
            [self.parent.tree.delete(i) for i in self.parent.tree.get_children()]
            [self.parent.tree.insert('', 'end', values=row) for row in self.c.fetchall()]
            self.conn.commit()
        self.destroy()
        self.parent.cache_entry.configure(state=tk.DISABLED)

    def viewPlan(self):
        for selected_item in self.tree.selection():
            vp.ViewPlan(self).initChild(str(self.tree.set(selected_item, '#1')))
        self.destroy()