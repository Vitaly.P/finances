from Main import *
import pandas as pd
from tkinter import *
from tkinter import messagebox as mb, filedialog as fd
from childs.plan.planstable import AddRecord as ar


class ViewPlan(tk.Toplevel):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent
        try:
            self.iconbitmap('imgs/childs/plan/ViewPlan.ico')
        except:
            pass

    def initChild(self, tableName):
        self.title('План для: ' + tableName)
        self.sharedTableName = tableName
        self.geometry('500x322+400+300')
        self.resizable(False, False)

        self.tree = ttk.Treeview(self, y=50, columns=('ID','month','sumOfPlan'),
                                 height=15, show='headings')

        self.mainMenu = tk.Menu()
        self.config(menu=self.mainMenu)
        # Меню файл
        self.filemenu = tk.Menu(self.mainMenu, tearoff=0)
        self.mainMenu.add_cascade(label="Файл", menu=self.filemenu)
        self.filemenu.add_command(label="Сохранить как Excel файл", command=self.extractData)
        self.filemenu.add_command(label="Закрыть окно", command=self.destroy)

        # Меню запись
        self.recordMenu = Menu(self.mainMenu, tearoff=0)
        self.mainMenu.add_cascade(label="Запись", menu=self.recordMenu)
        self.recordMenu.add_separator()
        self.recordMenu.add_command(label="Добавить запись", command=self.getRecordInstance)
        self.recordMenu.add_separator()
        self.recordMenu.add_command(label="Удалить выбранные записи", command=self.deleteSelectedData)
        self.recordMenu.add_separator()
        self.recordMenu.add_command(label="Удалить все записи", command=self.deleteAllRecords)
        self.recordMenu.add_separator()

        #Scrollbar
        self.yscrollbar = ttk.Scrollbar(self, orient=tk.VERTICAL)
        self.yscrollbar.grid(row=1, column=2, sticky=(tk.N, tk.S, tk.W, tk.E))
        self.yscrollbar.config(command=self.tree.yview)
        self.tree.config(yscrollcommand=self.yscrollbar.set)
        self.tree.column('ID', width=1, anchor=tk.CENTER)
        self.tree.column('month', width=100, anchor=tk.CENTER)
        self.tree.column('sumOfPlan', width=382, anchor=tk.CENTER)
        self.tree.heading('ID', text='№')
        self.tree.heading('month', text='Месяц')
        self.tree.heading('sumOfPlan', text='Запланированная сумма')
        self.tree.grid(row = 1, column = 0, columnspan=2)
        self.bind('<Escape>', lambda event: self.destroy())
        self.viewRecord()

    def getRecordInstance(self):
        ar.AddRecord(self).initChild(self.sharedTableName)
    def queryViewPlans(self):
        self.conn = sqlite3.connect('dbs/plans.db')
        self.c = self.conn.cursor()
        self.c.execute('''SELECT * FROM '''+ self.sharedTableName)
        [self.tree.delete(i) for i in self.tree.get_children()]
        [self.tree.insert('', 'end', values=row) for row in self.c.fetchall()]
        self.conn.commit()

    def queryOpenTable(self):
        self.parent.cache_entry.delete(0,"")
        self.conn = sqlite3.connect('dbs/plans.db')
        self.c = self.conn.cursor()
        for selected_item in self.tree.selection():
            self.c.execute('''SELECT * FROM ''' + str(self.tree.set(selected_item, '#1')))
            self.parent.cache_entry.insert(0, str(self.tree.set(selected_item, '#1')))
            [self.parent.tree.delete(i) for i in self.parent.tree.get_children()]
            [self.parent.tree.insert('', 'end', values=row) for row in self.c.fetchall()]
            self.conn.commit()
        self.destroy()
    def extractData(self):
        path = fd.asksaveasfilename(title="Сохранить как Excel файл",
                                    filetypes=((["XLSX файлы", "*.xlsx"]),),
                                    defaultextension=".xlsx")
        try:
            df = pd.read_sql('''SELECT date, sumOfPlan FROM ''' + self.sharedTableName,
                             sqlite3.connect('dbs/plans.db'))
            writer = pd.ExcelWriter(path, engine='xlsxwriter')
            df.to_excel(excel_writer=writer, sheet_name='Лист 1')
            writer.save()
            writer.close()
        except:
            pass


    def deleteAllRecords(self):
        self.conn = sqlite3.connect('dbs/plans.db')
        self.c = self.conn.cursor()
        if mb.askyesno("Удаление всех записей", "Вы уверены, что хотите удалить все записи из выбранной таблицы?"):
            if mb.askyesno("Удаление всех записей", "Вы точно уверены? Все данные из выбранной таблицы будут безвозрватно удалены."):
                self.c.execute('''DELETE FROM '''+ self.sharedTableName)
                self.conn.commit()
                self.viewRecord()


    def deleteSelectedData(self):
        if mb.askyesno("Удаление записей", "Удалить выделенные записи? Выбранные данные будут безвозрватно удалены."):
            for selected_item in self.tree.selection():
                self.conn = sqlite3.connect('dbs/plans.db')
                self.c = self.conn.cursor()
                self.c.execute('''DELETE FROM ''' + self.sharedTableName + ''' WHERE id=?''',(self.tree.set(selected_item, '#1'),))
                self.conn.commit()
                self.tree.delete(selected_item)
            self.viewRecord()


    def viewRecord(self):
        self.conn = sqlite3.connect('dbs/plans.db')
        self.c = self.conn.cursor()
        self.c.execute('''SELECT * FROM ''' + self.sharedTableName)
        [self.tree.delete(i) for i in self.tree.get_children()]
        [self.tree.insert('', 'end', values=row) for row in self.c.fetchall()]
        self.conn.commit()



