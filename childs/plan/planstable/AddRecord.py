import tkinter as tk
from tkinter import ttk
import datetime as dt
import sqlite3
import Main
from tkinter import *
from tkinter import messagebox as mb


class AddRecord(tk.Toplevel, ttk.Combobox):
    def __init__(self, parent):
        super().__init__()
        self.view = Main
        self.parent = parent
        try:
            self.iconbitmap('imgs/childs/plan/AddRecord.ico')
        except:
            pass

    def initChild(self, tableName):
        self.sharedTableName = tableName
        self.title('Добавление записи в таблицу с планами')
        self.geometry('400x180+400+300')
        self.resizable(False, False)

        self.months = [u'Январь', u'Февраль', u'Март', u'Апрель', u'Май', u'Июнь', u'Июль',
                       u'Август', u'Сентябрь', u'Октябрь', u'Ноябрь', u'Декабрь']
        self.monthsForCombo = [u'Январь', u'Февраль', u'Март', u'Апрель', u'Май', u'Июнь', u'Июль',
                               u'Август', u'Сентябрь', u'Октябрь', u'Ноябрь', u'Декабрь']



        self.label_sumOfPlan = tk.Label(self, text='Сумма')
        self.label_sumOfPlan.place(x=50, y=30)
        self.label_month = tk.Label(self, text='Месяц')
        self.label_month.place(x=50, y=60)
        self.label_year = tk.Label(self, text='Год')
        self.label_year.place(x=50, y=90)



        self.sumOfPlan = tk.Entry(self)

        self.sumOfPlan.place(x=200, y=30, width=143)

        self.month = ttk.Combobox(self, values=self.monthsForCombo)
        self.month.place(x=200, y=60,width=143)

        self.year = ttk.Entry(self)
        self.year.place(x=200, y=90, width=143)
        self.year.insert(0, dt.date.today().strftime("%Y"))

        for i in range (11):
            if dt.date.today().strftime("%m")=="0"+str(i):
                self.month.delete(0,END)
                self.month.insert(0, self.monthsForCombo.pop(i-1))

        self.btn_cancel = ttk.Button(self, text='Отмена', command=self.destroy)
        self.btn_cancel.place(x=300, y=130)
        self.btn_add = ttk.Button(self, text='Добавить')
        self.btn_add.place(x=220, y=130)
        self.now = dt.date.today()

        self.bind('<Return>', lambda event: self.getInputEntries())
        self.btn_add.bind('<Button-1>', lambda event: self.getInputEntries())
        self.bind('<Escape>', lambda event: self.destroy())
        #!!!!!!!!!!!!

    def getInputEntries(self):
        self.date = self.convertMonth()+'.'+self.year.get()
        self.insertData(self.sumOfPlan.get(), self.date)
        self.viewRecord()
        self.focus_set()
        self.grab_set()

    def viewRecord(self):
        self.conn = sqlite3.connect('dbs/plans.db')
        self.c = self.conn.cursor()
        self.c.execute('''SELECT * FROM ''' + self.sharedTableName)
        [self.parent.tree.delete(i) for i in self.parent.tree.get_children()]
        [self.parent.tree.insert('', 'end', values=row) for row in self.c.fetchall()]
        self.conn.commit()

    def insertData(self, sumOfPlan, date):
        planPattern = r'[^\.0-9]'

        if re.search(planPattern, self.sumOfPlan.get()) or self.sumOfPlan.get() == '':
            mb.showerror("Ошибка",
                     '''Введите корректную сумму. Сумма может содержать следуюшие символы "0-9, .".''')

        else:
            self.conn = sqlite3.connect('dbs/plans.db')
            self.c = self.conn.cursor()
            self.c.execute(
                '''SELECT date FROM ''' + self.sharedTableName + ''' WHERE date = ''' +date)
            check = self.c.fetchall()
            list = "[('" +self.date+"',)]"
            if str(check).__eq__(list):
                mb.showerror("Ошибка", "Одному месяцу может соответствовать только одна запланированная сумма!")
                self.conn.commit()
            else:
                self.c.execute('''INSERT INTO ''' + self.sharedTableName + ''' (date, sumOfPlan) VALUES(?, ?)''',(date, sumOfPlan))
                self.conn.commit()

    def convertMonth(self):
        convertedMonth = ""
        for i in range(12):
            if self.months.index('' + self.month.get() + '') == i:
                convertedMonth = str(i + 1)
        return convertedMonth